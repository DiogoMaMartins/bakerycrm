import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Header from '../../components/Header';
import DrawerMenu from '../../components/DrawerMenu';
import ListUsers from '../../components/ListUsers';

import useStyles from './styles.js';

const Customers: React.FC = () => {
	const classes = useStyles({});

  return(
      <div>
      	<Header/>

        <main>
        	<Container maxWidth="lg" className={classes.container}>
        		<div className={classes.appBarSpacer} />
        		<Grid container spacing={3}>
	        		<Grid item md={1} lg={1}/>
	    			<Grid item xs={12} md={11} lg={11}>
	  					<ListUsers/>
	  				</Grid>
        		</Grid>
        	</Container>
        </main>

      </div>
    );
}


export default Customers;
