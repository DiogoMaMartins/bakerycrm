import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Header from '../../components/Header';
import DrawerMenu from '../../components/DrawerMenu';
import PurchaseTemplate from '../../components/PurchaseTemplate';

import useStyles from './styles.js';

const Purchases: React.FC = () => {
	const classes = useStyles({});

  return(
      <div>
      	<Header/>
        <main>
        <div className={classes.appBarSpacer} />
        	<Container maxWidth="lg" className={classes.container}>
        		
        		<Grid container spacing={3}>
	        		<Grid item md={1} lg={1}/>
	    			<Grid item xs={12} md={11} lg={11}>
	  					<PurchaseTemplate/>
	  				</Grid>
        		</Grid>
        	</Container>
        </main>

      </div>
    );
}


export default Purchases;
