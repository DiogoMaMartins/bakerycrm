import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom'
import { Route,Switch,Redirect } from 'react-router';
import { isAuthenticated } from './auth';

import Home from '../NotConnected/Home';
import Products from '../Connected/Products';
import Customers from '../Connected/Customers';
import Purchases from '../Connected/Purchases';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
  {...rest}
    render={props =>
    isAuthenticated() ? (
      <Component {...props}/>
      ): (
      <Redirect to={{ pathname: '/', state: { from: props.location }}}/>
    )
    }
    />
  );
class Routes extends Component {
    render() {
      return (
              <BrowserRouter>
                <Switch>
                  <Route exact path='/' component={Home} />
                  <PrivateRoute exact path="/products" component={Products}/>
                  <PrivateRoute exact path="/customers" component={Customers}/>
                  <PrivateRoute exact path="/purchases" component={Purchases}/>
                  <Redirect to="/customers" />
                </Switch>
              </BrowserRouter>
      )
    }
}

export default Routes;
