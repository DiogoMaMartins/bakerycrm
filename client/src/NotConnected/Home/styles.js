import styled from 'styled-components';

export const Container = styled.div`
	background:#3F51B5;
	height:100%;
	width:100%;
	position:absolute;
	display:flex;
	flex-direction:column;
	align-items:center;
	justify-content:center;
`