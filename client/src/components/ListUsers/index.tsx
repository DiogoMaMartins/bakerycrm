import React,{ useState ,useEffect} from 'react';
import Grid from '@material-ui/core/Grid';
import MaterialTable from 'material-table';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import axios from 'axios';

   export interface IUSERS{
    _id:string,
    firstname:string,
    lastname:string,
    email:string,
    address:string,
    password:string,
    transactions?:any
  }



export interface IPRODUCT{
    id:string,
    title:string,
    description:string,
    active:boolean,
    price:number
  }


const ListUsers:React.FC = ( ) => {
	let usersData:any = [];
	let token = sessionStorage.getItem("token");
	const [users,setUsers] = useState<any>([])


async function get () {
      axios.get('https://bakery-api1.herokuapp.com/api/users/')
	   	.then(response =>response.data)
	   	.then(res => {
	   		 setUsers(res)
	   		 return res
	   	})
	   	.then(otherFetch => {

	   		 otherFetch.map(async (x:any, index:number) => {

	   		 	let userId = x._id

    		const response = await fetch(`https://bakery-api1.herokuapp.com/api/orders/getUsersAndTransaction/${userId}`)
    		const json = await response.json();
    		if(json.length >= 1){
    			let obj = {
	   		 		_id:x._id,
	   		 		firstname:x.firstname,
	   		 		lastname:x.lastname,
	   		 		email:x.email,
	   		 		address:x.address,
	   		 		password:x.password,
	   		 		transactions:json
	   		 	}

	   		 	usersData.push(obj)
    		}else{
    			let obj = {
	   		 		_id:x._id,
	   		 		firstname:x.firstname,
	   		 		lastname:x.lastname,
	   		 		email:x.email,
	   		 		address:x.address,
	   		 		password:x.password
	   		 	}

	   		 	usersData.push(obj)
    		}

	   		 })

	   	}).then(x => {
	   		setTimeout(() => {
	   			console.log("users data",usersData)
	   			setUsers(usersData)
	   		},500)


	   	})
	   	.catch(error => {
	   		console.log(error)
	   	})

    }

    useEffect(() => {
    	get()


    },[])

    async function deleteUsers (oldData:any) {
    	let userId = oldData._id;

    	axios.delete(`https://bakery-api1.herokuapp.com/api/users/delete/${userId}?token=${token}`)
    	.then(response => {
    		console.log("im response",response)
    		get()
    	})
    	.catch(err => {
    		console.log(err)
    	})
    }

    async function upadateUsers(data:any) {

    	axios.put(`https://bakery-api1.herokuapp.com/api/users?token=${token}`,{
    		id:data._id,
    		firstname:data.firstname,
    		lastname:data.lastname,
    		email:data.email,
    		address:data.address,
    		password:data.password,

    	})
    	.then(response => {
    		console.log(response)
    		get()
    	})
    	.catch(err => {
    		console.log(err)
    	})
    }


    const [state, setState] = React.useState<any>({
    columns: [
      { title: 'First Name', field: 'firstname' },
      { title: 'Last Name', field: 'lastname' },
      { title: 'Address', field: 'address' },
      { title: 'email', field: 'email' },
    ],
    data:users
  });
	return(
			<React.Fragment>
				<Grid container spacing={4}>
					<Grid item xs={12} md={10} lg={12}>
							<MaterialTable
						      title="Users"
						      columns={state.columns}
						      data={users}
						      editable={{
					        onRowUpdate: (newData, oldData) =>
					          new Promise(resolve => {
					            setTimeout(() => {
					            	resolve();
					            	//console.log("new data",newData)
					            	upadateUsers(newData)

					             /* resolve();
					              const data = [...state.data];
					              data[data.indexOf(oldData)] = newData;
					              setState({ ...state, data });*/
					            }, 600);
					          }),
					        onRowDelete: oldData =>
					          new Promise(resolve => {
					            setTimeout(() => {
					              resolve();
					              deleteUsers(oldData)
					              /*const data = [...users];
					              data.splice(data.indexOf(oldData), 1);
					              console.log("data",data)
					              //setUsers({ data });*/
					            }, 600);
					          }),

					      }}
						      detailPanel={[
    {

      render: (rowData:any) => {
     	console.log("transactions",rowData.transactions)


        return(

        	<div>
        	<Table size="small" style={{
					              fontSize: 100,
					              textAlign: 'center',
					              color: 'black',
					              backgroundColor: '#fff',
					            }}>
					        <TableHead>
					          <TableRow>
					            <TableCell>Date</TableCell>
					            <TableCell>Transaction Number</TableCell>
					            <TableCell>Product Name</TableCell>
					            <TableCell>Description</TableCell>
                      <TableCell>Quantity</TableCell>
					            <TableCell>Price</TableCell>
					          </TableRow>
					        </TableHead>
					        <TableBody >
        	{
        		rowData.transactions !== undefined ?

        		rowData.transactions.map( (x:any,index:number) => {

        			return ( x.items.map((y:any) => {
        				return (
					            <TableRow key={y.product._id}>
					           	  <TableCell>{x.createDate}</TableCell>
					              <TableCell>{x.number}</TableCell>
					              <TableCell>{y.product.title}</TableCell>
					              <TableCell>{y.product.description}</TableCell>
                        <TableCell>{y.product.quantity}</TableCell>
					              <TableCell align="right">{y.product.price}</TableCell>
					            </TableRow>
        						)
        			}))
        		})

        			:

        					<TableRow >
				           		 <TableCell>Don't have transactions</TableCell>
				            </TableRow>





        		}






        	</TableBody>
		      </Table>
        	</div>
        	)
      },
    }
  ]}
   />
					</Grid>
				</Grid>
			</React.Fragment>

		)
}

export default ListUsers;
