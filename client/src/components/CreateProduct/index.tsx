import React, {useState} from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Fab from '@material-ui/core/Fab';
import IconButton from '@material-ui/core/IconButton';
import NavigationIcon from '@material-ui/icons/Navigation';
import axios from 'axios';
import useStyles from './styles';


interface Product{
  title:string,
  description:string,
  price:number,
  image:any,
}


const CreateProduct:React.FC = () => {
  const classes = useStyles({});
  const [uploading,setUploading] = useState(false)
  const [image,setImage] = useState(null)
  const [file,setFile] = useState(null)
  const [description,setDescription] = useState("")
  const [price,setPrice] = useState(0)
  const [title,setTitle] = useState("")

  function sendfile(selectorFiles: FileList){
    setFile(selectorFiles[0])
    setImage(URL.createObjectURL(selectorFiles[0]))
    setUploading(true)
  }

  function post(){
    let token = sessionStorage.getItem('token')
    let pri = price.toString()
    const formData = new FormData();
				formData.append("image",file)
        formData.append("description",description)
        formData.append("price",pri)
        formData.append("title",title)

    axios.post(`https://bakery-api1.herokuapp.com/api/products?token=${token}`,formData)
    .then(response => alert(response.data.message))
    .then(refresh => {window.location.reload()})
    .catch(err => {
      console.log(err)
    })
  }
  return (
    <Paper>
      <h1>Post New Product</h1>
      <hr/>
      <Grid container spacing={4} >
        <Grid item xs={12} md={3} lg={3}>
        <div className={classes.dropzone} >

            <input  onChange={(e) => sendfile(e.target.files)} type="file"  className={classes.inputfield}  />

            {
              uploading ?

                <img src={image} className={classes.image}/>
                :
                <p>Pick one Picture</p>
            }

            </div>
        </Grid>
        <Grid item xs={12} md={7} lg={7}>
          <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="title"
              label="Title"
              type="title"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              id="title"
            />
              <TextareaAutosize  value={description}
              onChange={(e) => setDescription(e.target.value)} className={classes.textArea} aria-label="minimum height" rows={6} placeholder="Description" />
              <Fab
          variant="extended"
          size="small"
          color="primary"
          aria-label="add"
          className={classes.margin}
          onClick={() => post()}
        >
          <NavigationIcon className={classes.extendedIcon} />
          Upload Product
        </Fab>
            </Grid>
            <Grid item xs={12} md={2} lg={2}>
            <TextField
                variant="outlined"
                margin="normal"
                required
                name="price"
                label="Price"
                type="number"
                id="price"
                value={price}
                onChange={(e) => setPrice(parseInt(e.target.value))}
              />
                  </Grid>

      </Grid>
    </Paper>
  )
}

export default CreateProduct;
