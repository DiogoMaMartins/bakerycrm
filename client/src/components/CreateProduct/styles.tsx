import { makeStyles } from '@material-ui/core/styles';
export default makeStyles(theme => ({
  dropzone:{
		height:300,
    width:'100%',
		padding:10,
		position:'relative',
		cursor:'pointer',
		outline:'2px dashed #4c389b',
		outlineOffset:-10,
		background:'transparent',
		color:'dingray',
		display: 'flex',
		justifyContent:'center',
		alignItems: 'center',
		flexDirection: 'column',
  },
  margin: {
   margin: theme.spacing(1),
 },
 extendedIcon: {
   marginRight: theme.spacing(1),
 },
  textArea:{
    width:'100%',
    maxWidth:'100%'
  },
	inputfield:{
		opacity:0,
		width:'100%',
		height: 200,
		position: 'absolute',
		cursor: 'pointer',
		color:'transparent',
	},
  image:{
    width:'100%',
    height:'100%'
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  dense: {
    marginTop: theme.spacing(2),
  },

  call:{
    fontSize: 30,
    color:'#4c389b',
    textAlign: 'center',
  }



}));
