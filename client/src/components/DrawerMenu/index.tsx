import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import { mainListItems } from './listItems';
import useStyles from './styles.js'
import clsx from 'clsx';
interface Props {
      callback:(status:boolean) => void,
      openMenu:boolean
}

const DrawerMenu: React.FC<Props> = ({callback,openMenu}) => {


  const classes = useStyles({});

	return (
			 <Drawer
        style={{position:'absolute',top:100}}
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !openMenu && classes.drawerPaperClose),
        }}
        open={openMenu}
      >
        <Divider />
        <List component="nav">{mainListItems}</List>
        <Divider/>
      </Drawer>
		);
}

export default DrawerMenu;