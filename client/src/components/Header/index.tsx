import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';
import CustomizedMenus from './CustomizedMenus'



const Header: React.FC = () => {


  return(
    <AppBar position="absolute" >
        <Toolbar>
          {/*<IconButton
                      edge="start"
                      color="inherit"
                      aria-label="open drawer"
                      onClick={() => test()}
                     
                    >
                      <MenuIcon />
                    </IconButton>*/}
                    <CustomizedMenus/>

          <Typography component="h1" variant="h6" color="inherit" noWrap >
            BakeryCRM
          </Typography>
        </Toolbar>
      </AppBar>

    );
}

export default Header;