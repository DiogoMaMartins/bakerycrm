import React, { useState ,useEffect} from 'react';
import Grid from '@material-ui/core/Grid';
import MaterialTable from 'material-table';
import axios from 'axios';


  export interface IPRODUCT{
    id:string,
    title:string,
    description:string,
    active:boolean,
    price:number,
    image?:string
  }




  interface props {
    opt?:boolean,
    items?:(...args: any[]) => void
  }

const ListProducts:React.FC <props>= ({ opt,items} ) => {
	let token = sessionStorage.getItem("token");
  const [products,setProducts] = useState<IPRODUCT[]>([])
  const [selectedProducts,setSelectedProducts] = useState<IPRODUCT[]>([])

     async function getProducts () {
      const response = await fetch('https://bakery-api1.herokuapp.com/api/products');
      const product = await response.json();
        setProducts(product)

    }

     function upadateProduct (data:any) {
    	axios.put(`https://bakery-api1.herokuapp.com/api/products/update?token=${token}`,{
    		id:data._id,
    		title:data.title,
    		description:data.description,
    		active:data.active,
    		price:data.price,
        image:data.image

    	})
    	.then(response => {
        getProducts()
    	})
    	.catch(err => {
        alert("err")
    		console.log(err)
    	})
    }

     function deleteProduct (data:any) {
      let token = sessionStorage.getItem('token')
    	let id = data._id;

    	axios.delete(`https://bakery-api1.herokuapp.com/api/products/${id}?token=${token}`)
    	.then(response => {
    		console.log(response)
        getProducts()
    	})
    	.catch(err => {
    		alert("Sorry but we have orders with this product")
    	})
    }

    function pushItems(){
            items(selectedProducts)
    }
     useEffect(() => {
       if(selectedProducts !== undefined && selectedProducts.length >= 1){
          pushItems()
       }

}, [selectedProducts])

    useEffect(() => {
    getProducts()
}, [])

		const [state, setState] = React.useState<any>({
    columns: [
      { title: 'Title', field: 'title' },
      { title: 'Description', field: 'description' },
      { title: 'Price', field: 'price' },
    ],
  });

	return(
		<React.Fragment>
			<Grid container spacing={4}>
				<Grid item xs={12} md={10} lg={12}>
					<MaterialTable
					      title="Products"
                options={{
                  selection: opt
                }}
                onSelectionChange={(rows) => setSelectedProducts(rows)}
					      columns={state.columns}
					      data={products}
					      editable={{
					        onRowUpdate: (newData, oldData) =>
					          new Promise(resolve => {
					            setTimeout(() => {
					            	resolve();
					            	upadateProduct(newData)
					            }, 600);
					          }),
					        onRowDelete: data =>
					          new Promise(resolve => {
					            setTimeout(() => {
					              resolve();

					              deleteProduct(data)
					            }, 600);
					          }),

					      }}/>
				</Grid>

			</Grid>
		</React.Fragment>
		);
}

export default ListProducts;
