import React,  { useState,useEffect } from 'react';
import Paper from '@material-ui/core/Paper';
import useStyles from './styles.js';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import ListProducts from '../ListProducts';
import Button from '@material-ui/core/Button';
import axios from 'axios';

 export interface IUSERS{
    _id:string,
    firstname:string,
    lastname:string,
    email:string,
    address:string,
    password:string,
    transactions?:any
  } 


const PurchaseTemplate: React.FC= () => {
  let token = sessionStorage.getItem("token");
  const classes = useStyles({});
  const [users,setUsers] = useState<IUSERS[]>([])
   const [user,setUser] = useState<IUSERS[]>([])
  const [selectedUser,setSelectedUser] = useState<any>([""])
   const [items,setItems] = useState<any>([])

  function  selectedItems (e:any):void {
    console.log("im event ",e)
      setItems(e)
    }

  function createOrder () {
    let token = sessionStorage.getItem("token");

    if(items.length === 0 || user.length === 0){
      alert('Please select items  and select user')
    }else{
      let itemsArray:any = [];

      items.map((x:any) => {
        let obj = {
          price:x.price,
          product:x._id,
        }

        itemsArray.push(obj)

      })
      

        let data = {
          id:user[0]._id,
          items:itemsArray
        }

        axios.post(`https://bakery-api1.herokuapp.com/api/orders?token=${token}`,data)
      .then(response => {
          alert("order created with success")
      })
      .catch(error => {
        console.log(error)
      })

      }

  }

  function get () {
      axios.get('https://bakery-api1.herokuapp.com/api/users/')
       .then(response =>response.data)
       .then(res => {
          setUsers(res)
         
       })
       .catch(err => {
         console.log("err",err)
       })
     }

     useEffect(() => {
       let user = users.filter(x => x._id === selectedUser)
       setUser(user)
     },[selectedUser])

     useEffect(() => {
      get()

   
    },[])

     console.log(user)

  return(
     
    <div>
      <Paper className={classes.root}>
        <Typography variant="h5" component="h3">
          Create Order
        </Typography>
        <form className={classes.container} noValidate autoComplete="off">
            <Grid container spacing={3}>
              <Grid item xs={12} md={10} lg={10}>
{
                 user.length >= 1 && user !== undefined ?
                     <>
                     <TextField
                        required
                        id="standard-read-only-input"
                        label="First name"
                        defaultValue={user[0].firstname}
                        value={user[0].firstname}
                        className={classes.textField}
                        margin="normal"
                        InputProps={{
                          readOnly: true,
                        }}
                      />
                      <TextField
                        required
                        id="standard-read-only-input"
                        label="Last name"
                        defaultValue={user[0].lastname}
                        value={user[0].lastname}
                        className={classes.textField}
                        margin="normal"
                        InputProps={{
                          readOnly: true,
                        }}
                      />

                      <TextField
                        required
                        id="standard-read-only-input"
                        label="Email Address"
                        defaultValue={user[0].email}
                        value={user[0].email}
                        className={classes.textField}
                        margin="normal"
                        InputProps={{
                          readOnly: true,
                        }}
                      />
                      <TextField
                        required
                        id="standard-read-only-input"
                        label="Address"
                        defaultValue={user[0].address}
                        value={user[0].address}
                        className={classes.textField}
                        margin="normal"
                        InputProps={{
          readOnly: true,
        }}
                      />
                      </>
                      :
                      <h3>Please Select One User</h3>
                    }

              </Grid>
            <Grid item xs={12} md={2} lg={2}>
                {<FormControl className={classes.formControl}>
                                  <InputLabel htmlFor="age-helper">Select Users</InputLabel>
                                  <Select
                                    value={selectedUser}
                                    onChange={event => setSelectedUser(event.target.value)}
                                    input={<Input name="age" id="age-helper" />}
                                  >
                                    <MenuItem value={selectedUser}>
                                      <em>None</em>
                                    </MenuItem>
                                    {
                                      users.map(x => (
                                          <MenuItem key={x._id} value={x._id}>
                                            
                                            {x.firstname} {x.lastname}
                                          </MenuItem>
                                        ))
                                    }
                                   
                                  </Select>
                                  <FormHelperText>Some important helper text</FormHelperText>
                                </FormControl>
                
                
                              
                }  
               
            </Grid>
            <Grid item xs={12} md={12} lg={12}>
              <ListProducts opt={true} items={selectedItems}/>
            </Grid>
            <Button variant="contained" color="primary" onClick={createOrder} className={classes.button}>
              Create Order
            </Button>
            </Grid>
          </form>
      </Paper>
    </div>
    );
}

export default PurchaseTemplate;