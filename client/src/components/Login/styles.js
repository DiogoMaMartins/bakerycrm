import styled from 'styled-components';

export const Container = styled.div`
	background:#fff;
	height:80%;
	width:80%;
	border-radius:38px;
	display:flex;
	flex-direction:column;
	align-items:center;
	justify-content:space-around;
	position:absolute;
	align-selft:center;
`

export const Header = styled.header`
	position:absolute;
	top:0;
	left:0;
	right:0;
	background:transparent;
	display:flex;
	flex-direction:row;
	align-items:center;
	justify-content:space-between;
	width:100%;
	height:15%;
`;

export const CompanyName = styled.h3`
	margin-left:40px;
	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 48px;
	line-height: 56px;
	color:#3F51B5;
	text-align:center;
`;

export const Link = styled.h4`
	margin-right:40px;
	cursor:pointer;
	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 36px;
	line-height: 42px;

`;

export const Section = styled.section`
	display:flex;
	flex-direction:column;
	align-items:center;
	justify-content:space-between;

	input{
		border:none;
		border-bottom:2px solid #3F51B5;
		max-width:80%;
		font-family: Roboto;
		font-style: normal;
		font-weight: normal;
		font-size: 24px;
		line-height: 30px;
		margin-bottom:10px;
	}


`;

export const Private = styled.h1`
	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size:50px;
	text-align:center;


`;



export const Button = styled.button`
	width:200px;
	background: #3F51B5;
	border-radius: 38px;
	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 48px;
	line-height: 56px;
	color: #FFFFFF;
`;


