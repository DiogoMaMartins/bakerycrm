import React,{ useState } from 'react';
import { Container,Header,CompanyName,Section,Private,Button } from './styles';
import axios from "axios";
import { Redirect } from 'react-router-dom';


const Login: React.FC = () => {
	const [email, setEmail] = useState<string>('');
	const [password, setPassword] = useState<string>('');
	const [next, setNext] = useState<boolean>(false);
	const [redirect,setRedirect] = useState<boolean>(false);

function login(){
	axios.post("https://bakery-api1.herokuapp.com/api/users/signin", {
                email: email,
                password: password,
            })
            .then(function(response) {
            	console.log(response)
                let token = response.data.token;
                sessionStorage.setItem("token", token);
            })
            .then(() => setRedirect(true))
            .catch(function(error) {
                console.log(error);
            })
}
		if (redirect) {
            return <Redirect to='customers'/>;
        }

	return(
			<Container>
				<Header>
				<CompanyName>BakeryCRM</CompanyName>
				</Header>
				<Section>
					<Private>Private Access</Private>
					{
        				 !next ?
         						<>
				           		 <input placeholder="Email address" value={email} onChange={e => setEmail(e.target.value) } />
				           		 <Button onClick={() => setNext(true)}>Next</Button>
			           			</>
			           	 :
			           		<>
			             	 <input placeholder="password" value={password} type="password" onChange={e => setPassword(e.target.value)}/>
			             	 <Button onClick={() => login()}>Login</Button>
			             	</>
			       }

				</Section>

			</Container>
		)
}

export default Login;
