
## Table of contents

* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Difficulties](#difficulties)
* [Bakery](#bakery)

## Heroku:PS: Don't forget the first connection takes time because is a free server so when you make a request to connect with a user can take some time.

github https://github.com/DiogoMaMartins
I create also a small shop just to have some desing in the application kkkk https://bakery--shop.herokuapp.com/

## General info
This application is hosted in Heroku https://bakery-crm.herokuapp.com/, and in the total until the push of this readme i took 7 hours ,all application is working but after i need to create some validations and maybe create ecommerce with cleaner code now that i undestand the typescript

Email:bakery@gmail.com
Password:1234

#Cloudinary
email:visor@mailfile.net
password:bakeryCRM1-



##Technologies
Project is created with

* React Js
* Node Js
* Material Ui
* Node JS
* MongoDB
* Express
* Heroku
* Cloudinary

## Setup

To run this project, install it locally using npm:

```
$ cd ./client && npm i
$ cd ./server && npm i
$ npm start
```

## Difficulties

The only difficulty I had was with typescript because I had only used typescript a long time ago and when mistakes came up I didn't know why but now I know everything has to have a type 
It's like in life it's either woman or man or both :)

## Bakery
![Homepage](./ProjectPictures/HomePage.jpg  "Homepage")
![Customers](./ProjectPictures/Customers.jpg  "Customers")
![Products](./ProjectPictures/Products.jpg  "Order")
![Order](./ProjectPictures/Order.jpg  "Order")



