import React, { Component } from 'react';
import LoginForm from '../components/LoginForm';
import Header from '../components/Header';
export default class Login extends Component {
  render(){
    return(
      <div>
        <Header/>
        <main>
        <LoginForm/>
        </main>
      </div>
    );
  }
}
