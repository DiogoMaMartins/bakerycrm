import { action } from 'typesafe-actions';

import { LoginTypes,Login } from './types';
export const loginf = (data:Login[]) => action(LoginTypes.LOGIN,{data});
export const loginRequest = (data:Login[]) => action(LoginTypes.LOGIN_REQUEST,{data});
export const loginSuccess = (data:Login[]) => action(LoginTypes.LOGIN_SUCCESS,{data});
export const loginFailure = () => action(LoginTypes.LOGIN_FAILURE);
