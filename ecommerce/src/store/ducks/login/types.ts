export enum LoginTypes{
	LOGIN = '@login/LOGIN',
	LOGIN_REQUEST = '@login/LOGIN_REQUEST',
	LOGIN_SUCCESS = '@login/LOGIN_SUCCESS',
	LOGIN_FAILURE = '@login/LOGIN_FAILURE',
};


export interface Login{
	email:string,
  password:string,
	redirect?:boolean,
}


export interface LoginState{
	readonly data: Login[]
	readonly loading: boolean
	readonly error:	boolean
}
