import { Reducer } from 'redux';
import { LoginState,LoginTypes } from './types';

const INITIAL_STATE:LoginState = {
  data:[],
  error:false,
  loading:false,
};

const reducer:Reducer<LoginState> = (state = INITIAL_STATE,action) => {

switch(action.type){
  case LoginTypes.LOGIN_REQUEST:
    return { ...state, loading:true};
  case LoginTypes.LOGIN_SUCCESS:
  sessionStorage.setItem('token', action.payload.data);

  let obj = {
    redirect:true
  }
    return {
      ...state, loading:false, error:false, data:obj,
    }
    case LoginTypes.LOGIN:
      return {
        ...state, loading:false, error:false, data:action.payload.data,
      }
  case LoginTypes.LOGIN_FAILURE:
    return {
      ...state, loading:false,error:true,data:[],
    };
  default:
    return state;
}
};
export default reducer;
