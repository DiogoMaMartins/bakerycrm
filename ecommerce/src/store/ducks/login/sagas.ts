import { call, put } from 'redux-saga/effects';
import api from '../../../services/api';

import { loginSuccess,loginFailure } from './actions';

export function* login(data:any){
  try{
      let obj = {
        email:data.payload.data[0].email,
        password:data.payload.data[0].password
      }
      const response = yield call(api.post,'users/signin',obj)

    yield put (loginSuccess(response.data.token));
  }catch(err){
    yield put (loginFailure());
  }
}
