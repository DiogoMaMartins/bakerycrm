

export enum ItemsTypes{
    ADD_ITEM = '@items/ADD_ITEM',
    GET_ITEM = '@items/GET_ITEM',
    DELETE_ITEM = '@items/DELETE_ITEM',
    DELETE_ALL = '@items/DELETE_ALL',
};

export interface Items{
  _id:string
  title:string
  quantity:number
  price:number
  image:string
  product:string
}


export interface ItemsState{
  readonly data:Items[]
  readonly id: string
  readonly items:any
}
