import { action } from 'typesafe-actions';

import {  ItemsTypes,Items } from './types';

export const getItem = () => action(ItemsTypes.GET_ITEM);
export const addItem = (data:Items[],id:string,items:any) => action(ItemsTypes.ADD_ITEM,{id,items,data});
export const deleteItem = (items:Items[]) => action(ItemsTypes.DELETE_ITEM);
export const deleteAll = () => action(ItemsTypes.DELETE_ALL);
