import { all, takeLatest} from 'redux-saga/effects';

import { ProductsTypes } from './products/types';
import { LoginTypes } from './login/types';

import { load } from './products/sagas';
import { login } from './login/sagas';

export default function* rootSaga(){
	return yield all ([
			takeLatest(ProductsTypes.LOAD_REQUEST,load),
			takeLatest(LoginTypes.LOGIN_REQUEST,login)
		])
}
