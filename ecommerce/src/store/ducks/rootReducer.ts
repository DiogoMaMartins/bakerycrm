import { combineReducers } from 'redux';

import products from './products';
import items from './items';
import login from './login';

export default combineReducers({
	products,
	items,
	login,
});
