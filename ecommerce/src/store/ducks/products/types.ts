export enum ProductsTypes{
	LOAD_REQUEST = '@products/LOAD_REQUEST',
	LOAD_SUCCESS = '@products/LOAD_SUCCESS',
	LOAD_FAILURE = '@products/LOAD_FAILURE',
};


export interface Products{
	_id:string
	title: string
	description: string
	image:string
	price:number
}


export interface ProductsState{
	readonly data: Products[]
	readonly loading: boolean
	readonly error:	boolean
	readonly offset: number
}
