import { createStore,Store,applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './ducks/rootReducer';
import rootSaga from './ducks/rootSaga';
import { ProductsState } from './ducks/products/types';
import { ItemsState } from './ducks/items/types';
import { LoginState } from './ducks/login/types';



export interface ApplicationState{
	products: ProductsState,
	items:ItemsState,
	login:LoginState
}

const sagaMiddleware = createSagaMiddleware();

const store: Store<ApplicationState>  = createStore(rootReducer,applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga)
export default store;
