import React, { Component } from 'react';
import SignUpForm from '../components/SignUpForm';
import Header from '../components/Header';
export default class SignUp extends Component {
  render(){
    return(
      <div>
        <Header/>
        <main>
        <SignUpForm/>
        </main>
      </div>
    );
  }
}
