import React from 'react';
import logo from './logo.svg';
import './App.css';
import ProductsList from './components/ProductsList';
import { Provider } from 'react-redux';

import store from './store';


const App = () => <Provider store={store}><ProductsList/></Provider>

export default App;
