import React, { Component } from 'react';
import Header from '../components/Header';
import Carousel from '../components/Carousel';
import ProductsList from '../components/ProductsList';
//import CPagination from '../components/CPagination';

class Home extends Component {

  render(){
    return(
      <div style={{height:"100%"}}>
          <Header/>
      <main>
      <Carousel/>
      <br/><br/>
      <ProductsList/>
      </main>
      <footer style={{display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
    {/*  <CPagination/>*/}
      </footer>
      </div>
    )
  }
}
export default Home
