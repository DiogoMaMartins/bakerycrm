import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom'
import { Route,Switch} from 'react-router';
import Home from '../Home';
import Login from '../Login';
import SignUp from '../SignUp';
import Product from '../Product';

class Routes extends Component {
    render() {
      return (
              <BrowserRouter>
                <Switch>
                  <Route exact path='/' component={Home} />
                  <Route exact path='/login' component={Login} />
                  <Route exact path='/register' component={SignUp} />
                  <Product  path='/product' component={Product} />
                </Switch>
              </BrowserRouter>
      )
    }
}

export default Routes;
