import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ShareIcon from '@material-ui/icons/Share';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Button from '@material-ui/core/Button';
import { Products } from '../../store/ducks/products/types';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import * as ItemsActions from '../../store/ducks/items/actions';
import { Items } from '../../store/ducks/items/types';
import { ApplicationState } from '../../store';
import util from '../../services/util';
import useStyles from './styles'


interface StateProps{
    items: Items[],
    products: Products
}

interface DispatchProps{
  addItem(data:Items[],id:string,items:any):any
}

interface Ite{
  _id:string
  title:string
  quantity:number
  price:number
  image:string
  product:string
}


type Props = StateProps & DispatchProps

 const ProductItems:React.FC<Props> = ({ products,items,addItem }) => {
  const classes = useStyles();


  let addItems = (data:any) => {

    let obj: Ite = {
      _id:data._id,
      title:data.title,
      price:data.price,
      quantity:1,
      image:data.image,
      product:data._id
    }
    items.push(obj)
    addItem(items,data._id,obj)
  }

  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={
          <Avatar aria-label="recipe" className={classes.avatar}>
            {products.title[0].split(/\s/).reduce((response,word)=> response+=word.slice(0,1),'')}
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title={products.title}
        subheader="September 14, 2016"
      />
      <CardMedia
        className={classes.media}
        image={products.image}
        title={products.title}
      />
      <CardContent className={classes.description}>
        <Typography variant="body2" color="textSecondary" component="p">
          {products.description}
        </Typography>
      </CardContent>

      <CardActions >
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <Typography variant="h4" >
        {util.formatCurrency(products.price)}

      </Typography>
        <Button variant="contained"  className={classes.button} onClick={() => addItems(products)}>
      Add Item
    </Button>
      </CardActions>
    </Card>
  );
}

const mapStateToProps = (state: ApplicationState) => ({
  items: state.items.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(ItemsActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProductItems);
