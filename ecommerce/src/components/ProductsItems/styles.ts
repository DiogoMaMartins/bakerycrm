import { makeStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
export default makeStyles(theme => ({
  card: {
    maxWidth: 345,
    maxHeight:500,
    margin:20,
    cursor:"pointer",
  },
  media: {
    height: 0,
    margin:10,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  button: {
    margin: theme.spacing(2),
    backgroundColor:"#FF6008",
    borderRadius:"25px"
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
    cursor:"pointer",
  },
  description:{
    overflow:"hidden",
    height:20
  }
}));
