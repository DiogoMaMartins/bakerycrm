import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import useStyles from './styles';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Paper from '@material-ui/core/Paper';

import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import * as ItemsActions from '../../store/ducks/items/actions';
import { Items } from '../../store/ducks/items/types';
import { ApplicationState } from '../../store';
import util from '../../services/util';
import axios from 'axios';

let token = sessionStorage.getItem('token')

interface StateProps{
    items: Items[]
}

interface DispatchProps{
  getItem():void,
  deleteItem(items:Items[]):void,
  deleteAll():void,
}

type Props = StateProps & DispatchProps


const Basket:React.FC<Props> = ({items,getItem,deleteItem,deleteAll}) =>  {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);


  function handleClick(event:any) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  function createOrder(e:any){
    let token = sessionStorage.getItem('token')
    e.preventDefault()

    if(!token){
      console.log(token)
      alert("You need to be connected to buy sommething")
    }else{
      let obj = {
        items:items
      }
      console.log(token)


  axios.post(`https://bakery-api1.herokuapp.com/api/orders?token=${token}`,obj)
      .then(response => {

        alert("order created with success")
        deleteAll()

      })
      .catch(error => {
        console.log(error)
      })

          }


  }
  function remove(x:any){
    items.push(x)
    console.log("x",x)
  }

  return (
    <div >
      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
      <Badge badgeContent={items.length} color="secondary">
       <ShoppingCartIcon className={classes.shoppingCard}/>
       </Badge>
      </Button>
      <Menu
        className={classes.menus}
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}

      >

        {
          items !== undefined && items.length >= 0 ?
              items.map(x => (
                <MenuItem key={x._id}  className={classes.menus} onClick={handleClose}>
                    <div className={classes.shopitems}>
                    <section style={{display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-around'}}>
                      <img style={{ width:50,height:50}} src={x.image} alt={x.title}/>
                      <span>{x.title}</span>
                    </section>
                    {/*<IconButton onClick={() => remove(x)} aria-label="delete" className={classes.margin}>
                      <DeleteIcon />
                    </IconButton> */}
                    <br/>
                    </div>
                    {x.quantity} x {util.formatCurrency(x.price)}

                    </MenuItem>

              ))
              :
              <p>Your basket is empty</p>
        }

        <div  style={{flex:1,display:'flex',flexDirection:'column'}}>
        <div style={{display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-around'}}>
          <section>
          <h4 style={{marginBottom:0,paddingBottom:0}}>Total </h4>
          <span style={{marginTop:0,fontSize:10}}>Excluding costs of delivery </span>
          </section>
        <b>{util.formatCurrency(items.reduce((a, c) => (a + c.price * c.quantity), 0))}
        </b>
        </div>

          <Button onClick={(e) => createOrder(e)} variant="contained" size="small" style={{width:200,alignSelf:'center',backgroundColor:'#333333'}} color="primary" className={classes.button}>
            <p>Buy</p>
          </Button>
        </div>


      </Menu>
    </div>
  );
}


const mapStateToProps = (state: ApplicationState) => ({
  items: state.items.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(ItemsActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Basket);
