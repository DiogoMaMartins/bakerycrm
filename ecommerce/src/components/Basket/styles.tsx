import { makeStyles } from '@material-ui/core/styles';

export default makeStyles( theme =>({
  shoppingCard:{
    color:"#fff"
  },
  shopitems:{
      listStyle:'none',
      display:'flex',
      flexDirection:'row'
  },
  menus:{
    width: '100%',
    marginTop:50,
    minWidth: 360,
    margin:theme.spacing(1)
  },
  margin:{
    margin:theme.spacing(1)
  },
  button: {
    margin: theme.spacing(1),
  },
}));
