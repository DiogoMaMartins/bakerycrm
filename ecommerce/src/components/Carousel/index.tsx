import React from 'react';
import { CarouselProvider, Slider, Slide,   } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';

export default class Carousel extends React.Component {
  render() {
    return (
      <div   style={{maxHeight:"400px"}}>
      <CarouselProvider
        naturalSlideWidth={100}
        naturalSlideHeight={40}
        totalSlides={3}
        interval={6000}
        isPlaying={true}
      >
        <Slider>
          <Slide index={0}><img   alt="bakery" style={{width:'100%',height:'400px'}} src ="https://cdn.pixabay.com/photo/2018/03/01/09/33/laptop-3190194__340.jpg" /></Slide>
          <Slide index={1}><img   alt="bakery" style={{width:'100%',height:'400px'}}src="https://cdn.pixabay.com/photo/2017/03/13/17/26/ecommerce-2140604__340.jpg"/></Slide>
          <Slide index={2}><img   alt="bakery" style={{width:'100%',height:'400px'}}src="https://cdn.pixabay.com/photo/2018/03/20/09/22/shopping-3242593__340.jpg"/></Slide>
        </Slider>
      </CarouselProvider>
      </div>
    );
  }
}
