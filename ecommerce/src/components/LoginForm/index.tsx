import React, {useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import useStyles from './styles.js';
import axios from 'axios';

import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { Login } from '../../store/ducks/login/types';
import { ApplicationState } from '../../store';
import * as LoginActions from '../../store/ducks/login/actions';
import { Redirect } from 'react-router-dom';

interface StateProps {
  login:Login[]
}
interface DispatchProps {
  loginf(data:Login[]):void
  loginRequest(data:Login[]):void
}

interface Form{
  email:string
  password:string
  redirect?:boolean
}
type Props = StateProps & DispatchProps


const  LoginForm:React.FC<Props> = ({ login,loginf,loginRequest }) => {
  const classes = useStyles();
  const [email,setEmail] = useState<string>("");
  const [password,setPassword] = useState<string>("");
  const [redirect,setRedirect] = useState(false);

  let connection = (event:any) => {
    event.preventDefault();
    let obj: Form = {
      email:email,
      password:password,
      redirect:false,
    }
    axios.post('https://bakery-api1.herokuapp.com/api/users/signin',obj)
      .then(response => {
        sessionStorage.setItem('token',response.data.token);
      }).then(redirect => {
            setRedirect(true)
      })
      .catch(error => {
        console.log(error)
      })


    /*
    login.push(obj)

    loginf(login)
    loginRequest(login)*/
  }
  if(redirect){
    return <Redirect to="/" />
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            className={classes.textFieldEdit}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            autoComplete="current-password"
            className={classes.textFieldEdit}
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={(e) => connection(e)}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2" className={classes.textt}>
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link href="/register" variant="body2" className={classes.textt}>
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
      </Box>
    </Container>
  );
}

const mapStateToProps = (state: ApplicationState) => ({
  login:state.login.data,
});

const mapDispatchToProps = (dispatch:Dispatch) => bindActionCreators(LoginActions,dispatch);

export default connect(mapStateToProps,mapDispatchToProps)(LoginForm);
