import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import Grid from '@material-ui/core/Grid';
import { Products } from '../../store/ducks/products/types';
import { ApplicationState } from '../../store';
import Container from '@material-ui/core/Container';
import * as ProductsActions from '../../store/ducks/products/actions';
import ProductsItems from '../ProductsItems';

interface StateProps {
  products: Products[]
}

interface DispatchProps {
  loadRequest(): void
}

type Props = StateProps & DispatchProps

class ProductsList extends Component<Props> {
  componentDidMount() {
    const { loadRequest } = this.props;

    loadRequest();
  }

  render() {
    const { products } = this.props;
    return (
      <div style={{flexGrow: 1}}>
      <Container maxWidth="lg" >
      <Grid container spacing={4} direction="row" justify="space-around" alignItems="center">

      {products.map(products => (
        <ProductsItems key={products._id} products={products} />
      ))}

      </Grid>
      <br/>
      </Container>
      </div>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => ({
  products: state.products.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(ProductsActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProductsList);
