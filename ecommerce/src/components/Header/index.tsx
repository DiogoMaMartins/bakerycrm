import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Switch from '@material-ui/core/Switch';
import {  Link as RouterLink } from 'react-router-dom';
import Basket from '../Basket';
import useStyles from './styles';
//const AdapterLink = React.forwardRef((props,ref) => <RouterLink innerRef={ref} {..props} />);
const Header:React.FC = () =>  {
const token = sessionStorage.getItem('token');
const [state, setState] = React.useState({
    connected: true,
    disconnected: true,
  });
const [search, setSearch] = React.useState('')

  const handleChange = (name:string) => (event:any) => {
    setState({ ...state, [name]: event.target.checked });
  };
  const classes = useStyles();

  if(!state.connected){
    sessionStorage.removeItem('token');
  }

    return (
      <div className={classes.root}>
        <AppBar position="static" >
          <Toolbar className={classes.toolbarr}>


              <Typography className={classes.title} variant="h6" noWrap>
              <RouterLink className={classes.linkTo} to='/' >
                Bakery Shop
                  </RouterLink>
              </Typography>

            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <InputBase
                placeholder="Search…"
                value={search}
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
                inputProps={{ 'aria-label': 'search' }}
              />
            </div>

            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="open drawer"
            >
              <Basket/>
            </IconButton>
            {
              token ?
              <Switch
                 checked={state.connected}
                 onChange={handleChange('connected')}
                 value="connected"
                 inputProps={{ 'aria-label': 'secondary checkbox' }}
               />
                :
                <RouterLink className={classes.linkTo} to='/login'>Login/SignUp</RouterLink>
            }

          </Toolbar>

        </AppBar>
      </div>
    );
}
export default Header;
