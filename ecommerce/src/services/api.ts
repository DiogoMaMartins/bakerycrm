
import axios from 'axios';

const api = axios.create({
baseURL: 'https://bakery-api1.herokuapp.com/api',
});

export default api;
