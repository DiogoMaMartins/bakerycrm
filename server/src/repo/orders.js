const mongoose = require('mongoose');
const Order = mongoose.model("Order");
const User  = mongoose.model("User");


exports.get = async(data) => {
	let res = await Order.find({},'number status createDate quantity')
	.populate('customer',"firstname lastname email address")
	.populate('items.product');
	return res;
}
exports.getUsersAndTransaction = async (id) => {
	let res = await Order.find({
		customer:id
	},'number status createDate quantity')
	.populate('customer',"firstname lastname email address")
	.populate('items.product');
	return res;
}


exports.create = async(data) => {
	let order = new Order(data);
	await order.save();
}
