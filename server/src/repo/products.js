const mongoose = require('mongoose');
const Product = mongoose.model("Product");


exports.get = () => {
	return Product
		.find({
			active:true
		});
}

exports.create = (data) => {
	let product = new Product(data);
	return product.save();
}

exports.update = (id,data) => {
	console.log('data',data,'id',id)
	return Product
		.findByIdAndUpdate(id, {

			$set:{
				title:data.title,
				description:data.description,
				price:data.price,
				image:data.image
			}
		})
}

exports.delete = (id) => {
	return Product
		.findByIdAndRemove(id)
}
