'use strict';

const express = require('express');
const router = express.Router();
const controller = require('../controllers/products');
const authService = require('../middleware/auth.js');
const upload = require('../handlers/multer');

router.get('/',controller.getAllProducts);
router.post('/',authService.isAdmin,upload.single('image'),controller.register);
router.put('/:id',authService.isAdmin,controller.update);
router.delete('/:id',authService.isAdmin,controller.delete);

module.exports = router;
