'use strict';

const express = require('express');
const router = express.Router();
const controller = require('../controllers/users');
const authService = require('../middleware/auth');

router.get('/',controller.get);
router.post('/signup',controller.register);
router.post('/signin',controller.login);
router.put('/',authService.isAdmin,controller.update);
router.delete('/delete/:id',authService.isAdmin,controller.delete);

module.exports = router;