'use strict';

const express = require('express');
const router = express.Router();
const controller = require('../controllers/orders');
const authService = require('../middleware/auth');

router.post('/',authService.authorize,controller.post);
router.get('/',controller.get);
router.get('/getUsersAndTransaction/:id',controller.getUsersAndTransaction);

module.exports = router;
