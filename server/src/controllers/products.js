'use strict';

const mongoose = require('mongoose');
const Product = mongoose.model('Product');
const repository = require('../repo/products');
const Order = mongoose.model("Order");

//cloudinary
const upload = require('../handlers/multer');
require("dotenv").config()
const cloudinay = require('cloudinary')
require('../handlers/cloudinary')

exports.getAllProducts = (req,res,next) => {
	repository
		.get()
		.then(data => {
			res.status(200).send(data)
		})
		.catch(error => {
			res.status(400).send(error)
		});
}


exports.register = async (req,res,next) => {

	/*
	const host = req.host;
	const filePath = req.protocol + "://" + host + '/' + req.file.path;
	*/

	//now with cloudinary

	const result = await  cloudinay.v2.uploader.upload(req.file.path)

	let obj = await {
			title:req.body.title,
			description:req.body.description,
			price:req.body.price,
			image:result.secure_url
	}

	repository
		.create(obj)
		.then(x => {
			res.status(201).send({
				message: 'Product has been registered successfully'
			});
		})
		.catch(error => {
			res.status(400).send({
				message:'Error trying   registered  Product',
				data:e
			});
		});
}

exports.update = (req,res,next) => {
	console.log(req.body)
	repository
		.update(req.body.id,req.body)
		.then(x => {
			res.status(201).send({
				message:'Product has been updated successfully'
			});
		})
		.catch(error => {
			res.status(400).send({
				message:'Error',
				data:e
			});
		});
}


exports.delete = (req,res,next) => {
	Order.find()
  .then(orders=> {
	orders.map(x => {
			x.items.map(z => {
				if(z.product == req.params.id){
					res.status(500).send({
						message:'Sorry but i have orders with this project'
					})
				}else{
					repository
						.delete(req.params.id)
						.then(x => {
							res.status(200).send({
								message:'Product Removed'
							});
						})
						.catch(e => {
							res.status(400).send({
								message:'Error',
								data:e
							});
						});
				}
			})
		})
	})
	.catch(err => {
		console.log("im err",err,"err")
	})

}
