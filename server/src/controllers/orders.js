'use strict';

const repository = require('../repo/orders');
const guid = require('guid');
const authService = require('../middleware/auth');


exports.get = async(req,res,next) => {
	try{
		let data = await repository.get(req.params.id);
		res.status(200).send(data);
	}catch(e){
		res.status(500).send({
			message:'Error'
		})
	}
}
exports.post = async (req,res,next) => {
	try{

		const token = req.body.token || req.query.token || req.headers['x-access-token']

		const data = await authService.decodeToken(token);
		if(data.roles[0]== 'admin'){
			await repository.create({
			customer:req.body.id,
			number:guid.raw().substring(0,6),
			items:req.body.items
		})
		}else{

			await repository.create({
			customer:data.id,
			number:guid.raw().substring(0,6),
			items:req.body.items
		});
		}

		
		res.status(201).send({
			message:"Order created"
		});
	}catch(e) {
		res.status(500).send({
			message:'Error'
		});
	}
};

exports.getUsersAndTransaction = async(req,res,next) => {
	 repository
        .getUsersAndTransaction(req.params.id)
        .then(data => {
                res.status(200).send(data);
        })
        .catch(e => {
                res.status(400).send(e);
        });
}