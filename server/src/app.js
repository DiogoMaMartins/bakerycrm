const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const router = express.Router();
const mongoose = require('mongoose');
const config = require('../bin/config');
const cors = require('cors');
mongoose.connect(config.database,{ useNewUrlParser:true});
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
app.set('superNode-auth', config.configName);

app.use(cors())

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended:false}));

const Product = require('./models/products');
const User = require('./models/users');
const Order = require('./models/orders');

const users = require('./routes/users');
const products = require('./routes/products');
const order = require('./routes/orders');

app.use('/api/users',users)
app.use('/api/products',products)
app.use('/api/orders',order);

app.get('/',(req,res,next) => {
        res.status(200).send({
                title:"BakeryCRM Api",
                versio:"0.0.1"
        });
});


module.exports = app;
