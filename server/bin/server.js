'use strict'

const http = require('http');
const debug = require('debug')('nodestr:server');
const express = require('express');
var path = require('path');
const app = require('../src/app');

const port = normalizePort(process.env.PORT || '3001');
app.set('port',port);

const server = http.createServer(app);

server.listen(port);
server.on('error',onError);
server.on('listening',onListening);

var productsImages = path.join(__dirname,'../productsImages');

// viewed at http://localhost:8080
app.get('/productsImages', function(req, res) {
    res.sendFile(path.join(productsImages));
});

app.use('/productsImages', express.static(productsImages));

function normalizePort(val){
	const port = parseInt(val,10);

	if (isNaN(port)){
		return val;
	}

	if(port >= 0) {
		return port;
	}

	return false;
}

function onError(error){
	if(error.syscall !== 'listen'){
		throw error;
	}

	const bind = typeof port === 'string' ?
		'Pipe' + port :
		'Port' + port;

	switch (error.code){
		case 'EACCESS':
			console.error(bind + 'requires elevated privileges')
			process.exit(1);
			break;
		case 'EADDRINUSE':
			console.error(bind + 'is already in use');
			process.exit(1);
			break;
		default:
			throw error;
	}
}


function onListening() {
	const addr = server.address();
	const bind = typeof addr === 'string'
		? 'pipe' + addr
		: 'port' + addr.port;
	debug('Listening on' + bind);
}
